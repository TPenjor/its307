# -*- coding: utf-8 -*-
"""Todo1.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1IJPGFXEIsiAGkHmPjJWxB_KdvoKYEpgp
"""

import requests
from bs4 import BeautifulSoup

# Sending request 

url = "https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1970"

response = requests.get(url)
response

response.text

soup = BeautifulSoup(response.text)

#simple for loop to print all the title and artist
for song in soup.find_all('tr')[1:101]:
  rank = song.findAll('td')[0].contents[0]
  title = song.findAll('td')[1].a.string
  artist = song.findAll ('td')[2].a.string
  print(rank,title,'BY',artist)

print("---------------------------------------------")

#Using key and value 
dictionary = {}
for song in soup.find_all('tr')[1:101]:
  rank = song.findAll('td')[0].contents[0]
  title = song.findAll('td')[1].a.string
  artist = song.findAll ('td')[2].a.string
  dictionary[rank,title] = artist
print(dictionary)